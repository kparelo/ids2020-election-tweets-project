# 2020 US ELECTION TWEETS ANALYSIS

## Group members
1. Indrek Anissimov
2. Lemmo Lavonen
3. Karl Parelo


## Supervisor  
tba
## Course name 
Introduction to Data Science - LTAT.02.002 

# Overview
The goal of our project is to perform sentiment analysis on tweets concerning the US 2020 presidential election and report any intersting trends and findings.
